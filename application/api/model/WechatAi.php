<?php

/**
 * @Author: lujizhang
 * @Date:   2019-10-16 09:26:11
 * @Last Modified by:   lujizhang
 * @Last Modified time: 2019-11-04 13:58:27
 */
// https://developers.weixin.qq.com/doc/offiaccount/Intelligent_Interface/OCR.html

namespace app\api\model;
use think\Db;
use app\api\model\Wechat;

class WechatAi extends Wechat{
    
    function __construct(){
        parent::__construct();
    }

    public function semantic($param){
        $url = 'https://api.weixin.qq.com/semantic/semproxy/search?access_token='.$this->accessToken;
        /*
        {
        "query":"查一下明天从北京到上海的南航机票",
        "city":"北京",
        "category": "flight,hotel",
        "appid":"wxaaaaaaaaaaaaaaaa",
        "uid":"123456"
        }
         */
        $res = $this->httpRequest($url,$param);
        return $res;
    }

    /**
     * 身份证ocr
     * @param  [type] $img_url [description]
     * @return [type]          [description]
     */
    public function ocrIdCard($img_url){
        $data = null;
        if(strpos($img_url,'http')!==0){
            $data = array("img"=> new \CURLFile($img_url));
        }
        else $img_url = urlencode($img_url);
        $url = 'https://api.weixin.qq.com/cv/ocr/idcard?img_url='.$img_url.'&access_token='.$this->accessToken;
        $res = $this->httpRequest($url,$data);
        // var_dump($res,$this->accessToken);
        $data = json_decode($res,true);
        return $data;
        // if(isset($data['errcode']) && $data['errcode']===0){
        //     return $data;
        // }
        // else return false;
         
       /* {
           "errcode": 0,
            "errmsg": "ok",
            "type": "Front", //正面
            "name": "张三", //姓名
            "id": "123456789012345678", //身份证号码
            "addr": "广东省广州市XXX", //住址
            "gender": "男", //性别
            "nationality": "汉" //民族
        }*/  
        /*{
            "errcode": 0,
            "errmsg": "ok",
            "type": "Back", //背面
            "valid_date": "20171025-20271025", //有效期
        }*/
    }

    /**
     * 银行卡OCR
     * @param  [type] $img_url [description]
     * @return [type]          [description]
     */
    public function ocrBankCard($img_url){
        $img_url = urlencode($img_url);
        $url = 'https://api.weixin.qq.com/cv/ocr/bankcard?img_url='.$img_url.'&access_token='.$this->accessToken;
        $res = $this->httpRequest($url);
        $data = json_decode($res);
        if(isset($data['errcode']) && $data['errcode']===0){
            return $data;
        }
        else return false;
    }

    /**
     * 行驶证OCR
     * @param  [type] $img_url [description]
     * @return [type]          [description]
     */
    public function ocrDriving($img_url){
        $img_url = urlencode($img_url);
        $url = 'https://api.weixin.qq.com/cv/ocr/driving?img_url='.$img_url.'&access_token='.$this->accessToken;
        $res = $this->httpRequest($url);
        $data = json_decode($res);
        if(isset($data['errcode']) && $data['errcode']===0){
            return $data;
        }
        else return false;
    }

    /**
     * 驾照OCR
     * @param  [type] $img_url [description]
     * @return [type]          [description]
     */
    public function ocrDrivingLicense($img_url){
        $img_url = urlencode($img_url);
        $url = 'https://api.weixin.qq.com/cv/ocr/drivinglicense?img_url='.$img_url.'&access_token='.$this->accessToken;
        $res = $this->httpRequest($url);
        $data = json_decode($res);
        if(isset($data['errcode']) && $data['errcode']===0){
            return $data;
        }
        else return false;
    }

    /**
     * 营业执照ocr
     * @param  [type] $img_rul [description]
     * @return [type]          [description]
     */
    public function ocrBizLicense($img_rul){
        $img_url = urlencode($img_url);
        $url = 'https://api.weixin.qq.com/cv/ocr/bizlicense?img_url='.$img_url.'&access_token='.$this->accessToken;
        $res = $this->httpRequest($url);
        $data = json_decode($res);
        if(isset($data['errcode']) && $data['errcode']===0){
            return $data;
        }
        else return false;
    }
}