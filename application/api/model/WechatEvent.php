<?php

/**
 * @Author: lujizhang
 * @Date:   2019-10-16 09:26:11
 * @Last Modified by:   lujizhang
 * @Last Modified time: 2019-11-01 18:07:28
 */

namespace app\api\model;
use think\Db;
use app\api\model\Wechat;
use app\api\model\WechatMenu;
use app\api\model\WechatMessage;

class WechatEvent extends Wechat {
    public $wxReturn;

    function __construct($getToken=true){
        parent::__construct($getToken);
    }

    /**
     * 校验微信请求的签名 用于确定请求来自微信
     * @param  [type] $param [description]
     * @return [type]        [description]
     */
    public function checkSignature($param){
        $signature = $param["signature"];
        $timestamp = $param["timestamp"];
        $nonce = $param["nonce"];
        $token=$this->wx_msg_token;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
    }

    public function doEvent($wx_return=null){
        try{
            if(empty($wx_return)){
                $wx_return=$this->getWxReturn();
                
            }
            $wx_event=$wx_return->Event;
        }
        catch(\Exeption $e){
            subLog($e->getMessage(),'wx-return get data');
        }
        // $wx_return->unionid = "o3VhiuF0QTYzcRx6yZvx0zw_3lic";
        subLog($wx_event,'wx_event','wx_event');
        
        switch ($wx_event) {
            case 'subscribe': //关注
                $this->doSubscribe();
                break;
            case 'unsubscribe': //取消关注 (weixin_is_attention设 0)
                $this->doUnsubscribe();
                break;
            case 'SCAN': //扫描
                $this->doScan();
                break;
            case "VIEW"://菜单跳转
                $this->doView();
                break;
            case "CLICK"://菜单CLICK
                $this->doMenuClick();
                // responseToWeixin($wx_return,"");
                // doMenuResponse($wx_return);
                // doMenuClick($wx_return);

                break;
            case "LOCATION": // 地理位置信息
                $this->doLocation();
                break;
            default: //普通消息
                # code...
                //subLog("input",$wx_return);
                $this->autoResponse();
                break;
        }
    }

    /**
     * 获取微信接口返回值 处理xml格式的返回值为数组
     * @return [type] [description]
     */
    public function getWxReturn(){
        $post_data = empty($GLOBALS["HTTP_RAW_POST_DATA"]) ? file_get_contents("php://input") : $GLOBALS["HTTP_RAW_POST_DATA"];
        $wx_return=simplexml_load_string($post_data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $this->wxReturn = $wx_return;
        return $wx_return;
    }

    public function getDecryptWxReturn(){
        $wm = new WechatMessage;
        $post_data = $wm->getDecryptWxMessage();
        
        $wx_return=simplexml_load_string($post_data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $this->wxReturn = json_decode(json_encode($wx_return),true);
        // subLog($wxReturn,'messageEvent wxReturn','wechat');
        return $wx_return;
    }


    /**
     * 关注公众号时自动注册微信用户账号
     * @param  [type] $wx_return [description]
     * @return [type]            [description]
     */
    public function doSubscribe(){
        $wx_return = $this->wxReturn;
        $wxUser = $this->getWxUserFromApi($wx_return->FromUserName);
        /*
        do  something
        */

    }

    public function doUnsubscribe(){
        $wx_return = $this->wxReturn;
        /*
        do  something
        */
    }

    public function doScan(){
        $wx_return = $this->wxReturn;
        /*
        do  something
        */
    }

    public function doView(){
        $wx_return = $this->wxReturn;
        /*
        do  something
        */
    }

    public function doMenuClick(){
        $wx_return = $this->wxReturn;
        $redirectUrl=$wx_return->EventKey;
        /*
        do  something
        */
    }

    public function doLocation(){
        #$wx_return = $this->wxReturn;
        #$session('wx_Latitude')=$wx_return->Latitude;
        #$session('wx_Longitude')=$wx_return->Longitude;
        #$session('wx_Precision')=$wx_return->Precision;
        /*
        do  something
        */
    }


    /**
     * 普通消息的自动回复处理
     * @param  [type] $in_content [description]
     * @param  [type] $wx_return  [description]
     * @return [type]             [description]
     */
    function autoResponse($in_content,$wx_return){
        $wx_return = $this->wxReturn;
        $in_content = $wx_return->Content;
       
        $access_token = $this->accessToken();
        $in_content=strtolower($in_content);
        $mm = new WechatMessage;
        switch ($in_content) {
            case 'update':
                $mm->responseToWeixin($wx_return,"已提交更新菜单申请",$access_token);
                $this->updateMenu();
                break;
            
            default:
                
                break;

        }
    }



}
