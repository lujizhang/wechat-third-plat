<?php
/*
 * @Author: lujizhang
 * @Date: 2020-05-20 17:39:16
 * @LastEditors: lujizhang
 * @LastEditTime: 2020-11-25 15:06:13
 * @Descripttion: 
 * @version: 
 */

/**
 * @Author: lujizhang
 * @Date:   2019-10-16 09:26:11
 * @Last Modified by:   lujizhang
 * @Last Modified time: 2019-11-02 23:03:35
 */

namespace app\api\model;
use think\Db;
use app\api\model\Wechat;

class WechatJs extends Wechat{
    public $jsTicket = '';
    function __construct($appId){
        parent::__construct($appId);
    }

    public function getSignatureByThird($url,$noncestr,$timestamp){
        $wx_config = $this->wx_config;
        
        // 这里写你读取全局缓存jsTicket的代码
        $jsTicket = Db::table('sy_wx_auth_info')
            ->where('authorizer_appid="'.$this->wx_app_id.'" ')
            ->order('id desc')
            ->value('js_ticket');
        $this->jsTicket = $jsTicket;
        $string1="jsapi_ticket=".$jsTicket."&noncestr=".$noncestr.
            "&timestamp=".$timestamp."&url=".$url;
        $signature=sha1($string1);
        return $signature;
    }

    /**
     * 调用微信接口获取js_ticket
     * @param  integer $needReturn [description]
     * @return [type]              [description]
     */
    public function getJsTicketByThird($appId,$accessToken){
        
        $url=$this->wxApiBase."/ticket/getticket?access_token=".$accessToken."&type=jsapi";
        $res = $this->httpRequest($url);
        
        $content = json_decode($res,true);
        if($content['errcode']!=0 ){
            subLog($url,'getJsTicketByThird url','wechat');
            subLog($content,'getJsTicketByThird','wechat');
        }
        if(isset($content['ticket'])) {
            $js_ticket = $content['ticket'];
            $expired = date("Y-m-d H:i:s",time()+7200);
            Db::table('sy_wx_auth_info')
                ->where('authorizer_appid="'.$appId.'" ')
                ->update(['js_ticket'=>$js_ticket,'js_ticket_expired'=>$expired]);
            return $js_ticket;
        }
        else{
            return false;
        }
        
    }

}