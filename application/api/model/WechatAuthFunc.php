<?php
/*
 * @Author: lujizhang
 * @Date: 2020-10-26 10:49:43
 * @LastEditors: lujizhang
 * @LastEditTime: 2020-10-26 11:03:00
 * @Descripttion: 
 * @version: 
 */

/**
 * @Author: lujizhang
 * @Date:   2019-10-16 09:26:11
 * @Last Modified by:   lujizhang
 * @Last Modified time: 2019-11-04 13:58:27
 */
// https://developers.weixin.qq.com/doc/offiaccount/Intelligent_Interface/OCR.html

namespace app\api\model;
use think\Db;
use app\api\model\Wechat;

class WechatAuthFunc{
    /*
权限集ID	权限集中文名称	权限集中文描述	权限集互斥
1	消息管理权限	帮助公众号接收用户消息，进行人工客服回复或自动回复	否
2	用户管理权限	帮助公众号获取用户信息，进行用户管理	否
3	帐号服务权限	帮助认证、设置公众号，进行帐号管理	否
4	网页服务权限	帮助公众号实现第三方网页服务和活动	否
5	微信小店权限	帮助公众号使用微信小店	否
6	微信多客服权限	帮助公众号使用微信多客服	否
7	群发与通知权限	帮助公众号进行群发和模板消息业务通知	否
8	微信卡券权限	帮助公众号使用微信卡券	否
9	微信扫一扫权限	帮助公众号使用微信扫一扫	否
10	微信连WIFI权限	帮助公众号使用微信连WIFI	否
11	素材管理权限	帮助公众号管理多媒体素材，用于客服等业务	否
12	微信摇周边权限	帮助公众号使用微信摇周边	否
13	微信门店权限	帮助公众号使用微信门店	否
15	自定义菜单权限	帮助公众号使用自定义菜单	否
22	城市服务接口权限	帮助城市服务内的服务向用户发送消息，沉淀办事记录，展示页卡及办事结果页	否
23	广告管理权限	帮助广告主进行微信广告的投放和管理	否
24	开放平台帐号管理权限	帮助公众号绑定开放平台帐号，实现用户身份打通	是
26	微信电子发票权限	帮助公众号使用微信电子发票	否
27	快速注册小程序权限	帮助公众号快速注册小程序	否
33	小程序管理权限	可新增关联小程序，并对公众号已关联的小程序进行管理	否
34	微信商品库权限	帮助公众号商家导入、更新、查询商品信息，从而在返佣商品推广等场景使用	是
35	微信卡路里权限	为公众号提供用户卡路里同步、授权查询、兑换功能	否
44	好物圈权限	帮助公众号将物品、订单、收藏等信息同步至好物圈，方便用户进行推荐	否
46	微信一物一码权限	帮助公众号使用微信一物一码功能	否
47	微信财政电子票据权限	帮助公众号完成授权、插卡及报销	否
54	服务号对话权限	帮助公众号配置对话能力，管理顾问、客户、标签和素材等	是
66	服务平台管理权限	帮助公众号管理服务平台上购买的资源	否
*/
    public const MESSAGE = 1;
    public const USER = 2;
    public const ACCOUNT = 3;
    public const WEB =4;
    public const SHOP = 5;
    public const CMS = 6;
    public const NOTICE = 7;
    public const COUPON = 8;
    public const SCAN = 9;
    public const WIFI = 10;
    public const SOURCE = 11;
    public const SHAKE = 12;
    public const MALL = 13;
    public const MANU = 15;
    public const CITY = 22;
    public const AD = 23;
    public const OPEN_ACCOUNT = 24;
    public const RECIPE = 26;
    public const LIGHTAPP_REGIST = 27;
    public const LIGHTAPP_MANAGE = 33;
    public const STORE = 34;
    public const CALORIE = 35;
    public const FAVOR_THINGS = 44;
    public const GOODS_CODE = 46;
    public const FINANCE_E_RECIPE = 47;
    public const CHAT = 54;
    public const SERVICE_PLAT = 66;

    function __construct(){
        parent::__construct();
    }

    
}