<?php

/**
 * @Author: lujizhang
 * @Date:   2019-10-16 09:26:11
 * @Last Modified by:   lujizhang
 * @Last Modified time: 2019-11-01 17:01:21
 */

namespace app\api\model;
use think\Db;
use app\api\model\Wechat;

class WechatMenu extends Wechat {
    
    function __construct(){
        parent::__construct();
    }

    
    /**
     * 更新微信公众号菜单
     * @param  [type] $json_menu    [description]
     * @param  [type] $access_token [description]
     * @return [type]               [description]
     */
    public function updateWxMenu($json_menu){
        $url = $this->wxApiBase."/menu/create?access_token=".$this->accessToken;
        $result_menu = $this->httpRequest($url, $json_menu);
        subLog("result_menu",$result_menu);
        subLog("json_menu",$json_menu);
        return $result_menu;
    }

    /**
     * 获取当前使用的自定义菜单的配置
     * @return [type] [description]
     */
    public function getWxMenu(){
        $url = $this->wxApiBase."/get_current_selfmenu_info?access_token=".$this->accessToken;
        $result_menu = $this->httpRequest($url);
        return $result_menu;
    }

    public function deleteMenu(){
        $url = $this->wxApiBase."/menu/delete?access_token=".$this->accessToken;
        $result = $this->httpRequest($url);
        return $result;
    }

}