# 微信第三方平台全网发布测试PHP

#### 介绍
基于TP5的php版对应微信第三方平台全网发布测试。

#### 软件架构
TP5+PHP7.2


#### 安装教程


```
创建数据表
CREATE TABLE `sy_wx_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid所属名称',
  `wx_app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '微信第三方平台APP id',
  `wx_msg_token` varchar(50) DEFAULT NULL COMMENT '微信消息验证token',
  `wx_msg_aeskey` varchar(50) DEFAULT NULL,
  `wx_app_secret` varchar(255) DEFAULT NULL COMMENT '微信APP 密钥',
  `component_verify_ticket` varchar(200) DEFAULT NULL,
  `component_verify_ticket_expired` timestamp NULL DEFAULT NULL,
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT 'api token',
  `token_expired` timestamp NULL DEFAULT NULL COMMENT 'token过期时间表',
  `get_token_status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '0 未获取 1 获取中 2 获取完毕',
  `js_ticket` varchar(255) DEFAULT '' COMMENT '微信js ticket',
  `js_ticket_expired` timestamp NULL DEFAULT NULL COMMENT 'ticket过期时间',
  `get_ticket_status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '0 未获取 1 获取中 2 获取完毕',
  `callback_url` varchar(255) DEFAULT NULL,
  `preauth_back_url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 有效  2 无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sy_wx_auth_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `authorizer_appid` varchar(32) NOT NULL DEFAULT '',
  `authorizer_access_token` varchar(200) DEFAULT NULL,
  `expired` timestamp NULL DEFAULT NULL,
  `authorizer_refresh_token` varchar(200) DEFAULT NULL,
  `js_ticket` varchar(200) DEFAULT NULL,
  `js_ticket_expired` timestamp NULL DEFAULT NULL,
  `func_info` text,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `authorization_code` varchar(200) DEFAULT NULL,
  `authorization_code_expired` timestamp NULL DEFAULT NULL,
  `pre_auth_code` varchar(200) DEFAULT NULL,
  `authorized` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `authorizer_appid` (`authorizer_appid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
```


#### 使用说明

1.  将代码放入你的项目中
2.  设置第三方平台授权事件接收URL及消息与事件接收URL
3.  在数据表sy_wx_config中配置微信第三方平台相关参数

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
