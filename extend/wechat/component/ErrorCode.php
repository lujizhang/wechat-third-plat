<?php
/*
 * @Author: lujizhang
 * @Date: 2020-10-14 14:02:19
 * @LastEditors: lujizhang
 * @LastEditTime: 2020-11-25 13:32:12
 * @Descripttion: 
 * @version: 
 */
namespace wechat\component;

/**
 * error code 说明.
 * <ul>
 *    <li>-40001: 签名验证错误</li>
 *    <li>-40002: xml解析失败</li>
 *    <li>-40003: sha加密生成签名失败</li>
 *    <li>-40004: encodingAesKey 非法</li>
 *    <li>-40005: appid 校验错误</li>
 *    <li>-40006: aes 加密失败</li>
 *    <li>-40007: aes 解密失败</li>
 *    <li>-40008: 解密后得到的buffer非法</li>
 *    <li>-40009: base64加密失败</li>
 *    <li>-40010: base64解密失败</li>
 *    <li>-40011: 生成xml失败</li>
 *    <li>-40012: aes 解密失败结果为空</li>
 *    <li>-40013: aes 解密失败结果长度少于16个字符</li>
 * </ul>
 */


class ErrorCode
{
	public static $OK = 0;
	public static $ValidateSignatureError = -40001;
	public static $ParseXmlError = -40002;
	public static $ComputeSignatureError = -40003;
	public static $IllegalAesKey = -40004;
	public static $ValidateAppidError = -40005;
	public static $EncryptAESError = -40006;
	public static $DecryptAESError = -40007;
	public static $IllegalBuffer = -40008;
	public static $EncodeBase64Error = -40009;
	public static $DecodeBase64Error = -40010;
	public static $GenReturnXmlError = -40011;
	// 以下自定义添加
	public static $DecryptEmpty = -40012;
	public static $ResultLengthLT16 = -40013;
}

?>